using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;


public class GameManager : MonoBehaviour
{
    public static bool GameOver;
    public static int SelectordeNivel;
    public static bool GanarNivel;
    public static bool EscenaPerder;

    void Start()
    {
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Y))
        {
            SceneManager.LoadScene("Winer");
        }

        if (EscenaPerder == true && Input.GetKeyDown(KeyCode.R))
        {
            ComenzarJuego();
        }
        if (EscenaPerder == true && Input.GetKeyDown(KeyCode.M))
        {
            SceneManager.LoadScene("Menu");
        }
        if (GanarNivel == true && Input.GetKeyDown(KeyCode.P))
        { CambiodeNivel(); }
        
    }

    private void ComenzarJuego()
    {
        if (SelectordeNivel == 1)
        {
            GameOver = false;
            
            Time.timeScale = 1;
            SceneManager.LoadScene("Nivel1");
        }
          
        if (SelectordeNivel == 2)
        {
            GameOver = false;
            Time.timeScale = 1;
            SceneManager.LoadScene("Nivel2");
        }
          
        if (SelectordeNivel == 3)
        {
            GameOver = false;
            Time.timeScale = 1;
            SceneManager.LoadScene("Nivel3");        }
            
       
    }
    private void CambiodeNivel()
    {
        if (SelectordeNivel == 1 && GanarNivel == true)
        {
         SceneManager.LoadScene("Nivel2"); // CargarNivel2
        }

        if (SelectordeNivel == 2 && GanarNivel == true)
        {
            SceneManager.LoadScene("Nivel3"); // CargarNivel3
        }

        if (SelectordeNivel == 3 && GanarNivel == true)
        {
            SceneManager.LoadScene("Creditos"); // CargarNivel3
        }


    }


    // FUNCIONES DEL MENU
    
    public void Niveluno()
    {
    SceneManager.LoadScene("Nivel1");
    }

    public void Niveldos()
    {
        SceneManager.LoadScene("Nivel2");
    }

    public void Niveltres()
    {
        SceneManager.LoadScene("Nivel3");
    }

    public void MenuPrincipal()
    {
        SceneManager.LoadScene("Menu");
    }

    public void Creditos()
    {
        SceneManager.LoadScene("Creditos");
    }

    public void Salir()
    {
        Application.Quit(); // salir de la app
    }

}


