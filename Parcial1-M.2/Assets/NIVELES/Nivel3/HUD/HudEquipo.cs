using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class HudEquipo : MonoBehaviour
{
    public Sprite Arma;
    public Sprite Vacio;
    public Text ContadorMunicion;
    public Text TiempoFinal;
    public GameObject ImagenFinal;
    public Color Transparente;
    
    void Start()
    {
        
    }

   
    void Update()
    {
        Equipamiento();
        ActualizarContadores();
        MostrarContadores();

    }

    private void Equipamiento()
    {
        switch (ControlJugador3.TipoArma) // VERIFICAMOS QUE TIPO DE ARMA TIENE Y COLOCAMOS EL SPRITE
        {
            case 0: 
            gameObject.GetComponent<Image>().sprite = Vacio;
            
            break;
            case 1: 
            gameObject.GetComponent<Image>().sprite = Arma;
            break;
        }
        
         
    }
    private void MostrarContadores()
    {
        if (ControlJugador3.TipoArma != 1)
        {
            ContadorMunicion.GetComponent<Text>().color = Transparente;
        }
        else { ContadorMunicion.GetComponent<Text>().color = Color.red; }

        if (Nivel3.ActivarTiempoFinal == true)
        {
            TiempoFinal.GetComponent<Text>().color = Color.red;
            ImagenFinal.SetActive(true);
        }
        else { TiempoFinal.GetComponent<Text>().color = Transparente;
            ImagenFinal.SetActive(false);
        }

    }
    private void ActualizarContadores()
    {
        //if (ControlJugador3.TipoArma == 1)
        ContadorMunicion.text = ControlJugador3.Municion.ToString();
        TiempoFinal.text = Nivel3.TiempoFinal.ToString();
    }
}
