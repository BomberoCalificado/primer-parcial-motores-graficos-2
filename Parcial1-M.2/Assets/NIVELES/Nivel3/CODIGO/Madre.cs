using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Madre : MonoBehaviour
{
    public float RangoAlerta;
    public float RangoSeguro;

    public LayerMask CapaJugador;
    public bool Alerta;
    public static bool Seguir;
    public static bool Liberada;
    public bool Asalvo;
    public float VelocidadMov;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        PermanecerCerca();
        if (Asalvo == false)
        {
         Perseguir();
        }
        
    }

    private void Perseguir()
    {
        Alerta = Physics.CheckSphere(transform.position, RangoAlerta, CapaJugador);
        if (Alerta == true && Seguir == true && Liberada == true)
        {
            Vector3 UbicacionJugador = new Vector3(Nivel3.MadreResguardo.transform.position.x, transform.position.y, Nivel3.MadreResguardo.transform.position.z);
            transform.LookAt(UbicacionJugador);
            transform.position = Vector3.MoveTowards(transform.position, UbicacionJugador, VelocidadMov * Time.deltaTime);
        }

    }

    private void PermanecerCerca()
    {     
      Asalvo = Physics.CheckSphere(transform.position, RangoSeguro, CapaJugador);
         }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position,RangoAlerta);
        Gizmos.DrawWireSphere(transform.position, RangoSeguro);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("SensorFinal"))
        {
            
            
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("SensorFinal"))
        {
            Nivel3.ActivarTiempoFinal = true;
        }

        if (other.CompareTag("SensorFinal"))
        {
            Nivel3.MadreEnPuertaFinal = true;
        }

        
            if (other.CompareTag("Agua"))
            {
            ControlJugador3.Vida = 0;
            }
        }
}
