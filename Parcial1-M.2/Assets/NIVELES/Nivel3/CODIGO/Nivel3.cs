using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Nivel3 : MonoBehaviour
{
    // control de UBICACIONES
    public Transform UbicacionResguardo;
    public static Transform MadreResguardo;
    public static Transform UbicarJugador;

    //CONTROL DE SITUACION NIVEL
    public static bool Ganaste;
    public static bool Perdiste;

    //CONTROL PUERTA 1
    public static int EnemigosHabitacion1;
    public GameObject PuertaFinal;
    // CONTROL PUERTA CELDA
    public static int EnemigosCelda;

    // CONTROL SITUACION FINAL
    public static float TiempoFinal;
    public Vector3 AbrirPuertaFinal;
    public Vector3 CerrarPuerta2;
    public static bool ActivarTiempoFinal;
    public GameObject Puerta2;
    public GameObject EnemigosFinal;
    public static bool MadreEnPuertaFinal;


 
    void Start()
    {
        GameManager.GameOver = false;
        GameManager.SelectordeNivel = 3;
        Ganaste = false;
        Perdiste = false;
        TiempoFinal = 40;
        ControlJugador3.Envenenado = false;
               
    }


    void Update()
    {
        ControlPuertas();
        ControlNivel();
        UbicarJugador = ControlJugador3.UbicacionJugador;
        MadreResguardo = UbicacionResguardo;
    }

    public void ControlNivel()
    {
        if (ControlJugador3.Vida <= 0)
        {
            Perdiste = true;
            GameManager.GameOver = true;
            SceneManager.LoadScene("GameOver");
         }
        if (Ganaste == true &&  MadreEnPuertaFinal == true)
        {
          SceneManager.LoadScene("Creditos");
        }
    }

    public void ControlPuertas()
    {
        if (ActivarTiempoFinal == true)
        {
            Puerta2.transform.position = CerrarPuerta2;
            BotonP1.Abierta = false;
            TiempoFinal = TiempoFinal - Time.deltaTime;
            EnemigosFinal.SetActive(true);
        }
        if (EnemigosCelda >= 3)
        {
            BotonCelda.Activado = true;
        } // PUERTA DE CELDA
        if (EnemigosHabitacion1 >= 3)           
        {
            BotonP1.Activado = true;
        } // PUERTA 1
        if (TiempoFinal <= 0)
        {
            PuertaFinal.transform.position = AbrirPuertaFinal;
        } // PUERTA FINAL
    }
}
