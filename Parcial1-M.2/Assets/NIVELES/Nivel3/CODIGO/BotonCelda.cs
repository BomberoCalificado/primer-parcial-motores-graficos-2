using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BotonCelda : Interactuable // HEREDA DE INTERACTUABLE
{
   public static bool Activado;

    private void Update()
    {
        if(Activado == true)
        {
        gameObject.transform.GetComponent<Renderer>().material.color = Color.green;
        }
    }

    public override void FuncionInteractuar() // REMPLAZAMOS ESTE METODO
    {
        base.FuncionInteractuar();        
        if (Activado == true && Input.GetKeyDown(KeyCode.E))
        {
         PuertaFuncional.Abierta = !PuertaFuncional.Abierta;
         Madre.Liberada = true;
        }       
    }

}
