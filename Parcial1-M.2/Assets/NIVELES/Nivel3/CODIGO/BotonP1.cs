using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BotonP1 : Interactuable // HEREDA DE INTERACTUABLE
{
    public static bool Activado;
    public GameObject Puerta;
    public static bool Abierta;
    public bool InfoApertura;
    private Vector3 PosicionInicial;
    public Vector3 PosicionNueva;

    private void Start()
    {
     PosicionInicial = Puerta.transform.position;
    }
    private void Update()
    {
        if (Activado == true)
        {
            gameObject.transform.GetComponent<Renderer>().material.color = Color.green;
        }
        AbrirPuerta();
    }

    public override void FuncionInteractuar() // REMPLAZAMOS ESTE METODO
    {
        base.FuncionInteractuar();
        if (Activado == true && Input.GetKeyDown(KeyCode.E))
        {
            Abierta = !Abierta;         
        }
    }
    public void AbrirPuerta()
    {
        if (Abierta == false)
        {
           Puerta.transform.position = PosicionInicial;
        }

        if (Abierta == true)
        {
           Puerta.transform.position = PosicionNueva;
        }
    }


}
