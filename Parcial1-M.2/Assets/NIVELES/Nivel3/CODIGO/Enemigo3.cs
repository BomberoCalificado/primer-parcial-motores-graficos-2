using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemigo3 : MonoBehaviour
{   
    // RECURSOS ATAQUE
    public GameObject Arma;
    public Transform Instanciador;
    public float VelocidadArma;
    public float Contador;    
    public bool Dispare;
    public float Vida;
    public Transform Ver;
    // RECURSOS MOVIMIENTO
    public float ControlVertical;
    public bool Arriba;
   
    void Start()
    {
        Vida = 100;
    }


    void Update()
    {
        Ver = ControlJugador3.UbicacionJugador;
        MovimientoVertical();
        

        if (Vida <= 0)
        {
            Nivel3.EnemigosHabitacion1 = Nivel3.EnemigosHabitacion1 + 1;
            Destroy(gameObject);
        }

    }

    public void ControlarAtaque()
    {
        Contador = Contador + Time.deltaTime;
        transform.LookAt(Ver);


        if (Contador >= 2)
        { Dispare = false; }
        if (Dispare == false)
        {
            Atacar();
        }
    }
    public void Atacar() // Disparar veneno
    {
        

        Instantiate(Arma, Instanciador.position, Instanciador.rotation).GetComponent<Rigidbody>().AddForce(transform.forward * VelocidadArma * Time.deltaTime, ForceMode.Impulse);
        Dispare = true;
        Contador = 0;
    }
    public void MovimientoVertical()
    {
       
        if (Arriba == false)
        {
            ControlVertical = ControlVertical + Time.deltaTime;
            if (ControlVertical <=3)
            gameObject.transform.position = transform.position + new Vector3(0, ControlVertical, 0) * Time.deltaTime;
            if (ControlVertical >= 3 && ControlVertical <= 6)
                ControlarAtaque();
            if (ControlVertical >= 6)
            {
                Arriba = true;
                ControlVertical = 3;
            }
            
        }
               
        if (Arriba == true)
        {
            ControlVertical = ControlVertical - Time.deltaTime;            
            gameObject.transform.position = transform.position - new Vector3(0, ControlVertical, 0) * Time.deltaTime;
            if (ControlVertical <= 0)
             Arriba = false;
        }

    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Cuchillo"))
        {
            Vida = Vida - 20;
        }
    }
}
