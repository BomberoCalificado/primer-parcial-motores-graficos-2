using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemigo2Nivel3 : MonoBehaviour
{
    // RECURSOS DE ATAQUE
    public float VelocidadGiro;
    public float ControladorApertura;
    public bool BrazosCerrados;
    public Transform Jugador;
    public GameObject BrazoIzquierdo;
    public GameObject BrazoDerecho;
    public LayerMask CapaJugador;
    public float RangoAlerta;
    public float Vida;
    // RECURSOS DE ESTADO
    public static bool Alerta;

    //MOVIMIENTO
    public float VelocidadMov;


    void Start()
    {
        Vida = 100;
    }

    void Update()
    {
        DetectarJugador();
        if (Alerta == true)
        {
            Perseguir();
        }
        else { gameObject.transform.GetComponent<Renderer>().material.color = Color.black; }

        if (Vida <= 0)
        {
            Nivel3.EnemigosCelda = Nivel3.EnemigosCelda + 1;
            Destroy(gameObject);
        }
    }

    private void Moverbrazos()
    {
        ControlarBrazos();
        if (BrazosCerrados == false) // ABRIR BRAZOS
            BrazoIzquierdo.transform.Rotate(new Vector3(0, -1, 0) * VelocidadGiro * Time.deltaTime);
        if (BrazosCerrados == true) // CERRAR BRAZOS
            BrazoIzquierdo.transform.Rotate(new Vector3(0, +1, 0) * VelocidadGiro * Time.deltaTime);

        if (BrazosCerrados == false) // ABRIR BRAZOS
            BrazoDerecho.transform.Rotate(new Vector3(0, +1, 0) * VelocidadGiro * Time.deltaTime);
        if (BrazosCerrados == true) // CERRAR BRAZOS
            BrazoDerecho.transform.Rotate(new Vector3(0, -1, 0) * VelocidadGiro * Time.deltaTime);


    }

    public void ControlarBrazos()
    {
        ControladorApertura = ControladorApertura + Time.deltaTime;
        if (ControladorApertura <= 1.30f)
        {
            BrazosCerrados = false;

        }
        if (ControladorApertura >= 1.30f && ControladorApertura <= 2.60f)
        {
            BrazosCerrados = true;
        }
        if (ControladorApertura > 2.60)
        {
            ControladorApertura = 0;
        }
    }

    public void Perseguir()
    {

        gameObject.transform.GetComponent<Renderer>().material.color = Color.red;
        transform.LookAt(Jugador); // mantener la mirada hacia el jugador
        transform.position = Vector3.MoveTowards(transform.position, new Vector3(Jugador.position.x, Jugador.position.y, Jugador.position.z), VelocidadMov * Time.deltaTime);
        Moverbrazos();
    } // si estoy alerta observo al enemigo

    private void DetectarJugador()
    {
        Alerta = Physics.CheckSphere(transform.position, RangoAlerta, CapaJugador);

    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, RangoAlerta);

    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Cuchillo"))
        {
            Vida = Vida - 10;
        } 
    }
}
