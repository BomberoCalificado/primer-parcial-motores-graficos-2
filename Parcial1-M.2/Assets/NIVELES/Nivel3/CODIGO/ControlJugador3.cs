using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlJugador3 : MonoBehaviour
{
    // recursos de movimiento
    public Rigidbody Rb;
    public float Fuerza;
    public float Velocidad;
    public Vector2 Smouse;
    public float VelocidadMovimiento;
    public Transform Cam;
    // INTERACCIONES   
    public float DistanciaRayo;
    private RaycastHit InfoRayo;
    public LayerMask ObjetosInteracciones;
    // RECURSOS DE VITALIDAD
    public static float Vida;
    public float VidaInfo;
    public float Armadura;
    public static bool Envenenado;
    public float ContadorEnvenenamiento;

    // RECURSOS EQUIPO
    public static int TipoArma;
    public Transform Instanciador;
    public GameObject Arma;
    // PISTOLA
    public static int Municion;
    public float TiempoEntreDisparos;
    public static bool ArmaCargada;
    public bool ArmaRecolectada;
    public float VelocidadBala;
    // GRANADA
    public static int CantidadGranadas;
    public static bool GranadaRecolectada;

    public static Transform UbicacionJugador;


    void Start()
    {
        Municion = 0;
        TipoArma = 0;
        Vida = 100;
        Rb = GetComponent<Rigidbody>();
        Cursor.lockState = CursorLockMode.Locked; // hace que el cursos no se mueva por toda la pantalla, osea queda fijo en una posicion
    }


    void Update()
    {
        MoverJugador();
        RotacionJugador();
        ElegirEquipo();
        Equipamiento();
        OrdenesMadre();
        Interacciones();

        if (Envenenado == true)
        {
            Envenenamiento();
        }

        UbicacionJugador = gameObject.transform;
        VidaInfo = Vida;
    }

    // MOVIMIENTO
    public void MoverJugador()
    {
        float H = Input.GetAxisRaw("Horizontal");
        float V = Input.GetAxisRaw("Vertical");
        Vector3 Velocidades = Vector3.zero;

        if (H != 0 || V != 0) // si algun valor es diferente a 0 entonces el jugador se esta moviendo
        {
            Vector3 Direccion = (transform.forward * V + transform.right * H).normalized;
            Velocidades = Direccion * VelocidadMovimiento * Time.deltaTime;
        }
        Velocidades.y = Rb.velocity.y;
        Rb.velocity = Velocidades;

    }
    public void RotacionJugador()
    {
        float RotacionH = Input.GetAxis("Mouse X");
        float RotacionV = Input.GetAxis("Mouse Y");
        if (RotacionH != 0)
        {
            transform.Rotate(0, RotacionH * Smouse.x * Time.deltaTime, 0);
        }
        if (RotacionV != 0)
        {
            Cam.Rotate(-RotacionV * Smouse.y * Time.deltaTime, 0, 0);
        }
    }
    // ARMAMENTO
    public void ComportamientoPistola()
    {
        DispararArma();
        if (ArmaCargada == false)
        {
            CaracteristicaDisparo();
        }

    }
    public void DispararArma()
    {
        if (TipoArma == 1 && Municion >= 1 && ArmaCargada == true && Input.GetKeyDown(KeyCode.Mouse0))
        {
            Instantiate(Arma, Instanciador.position, Instanciador.rotation).GetComponent<Rigidbody>().AddForce(transform.forward * VelocidadBala * Time.deltaTime, ForceMode.Impulse);
            Municion = Municion - 1;
            ArmaCargada = false;
        }
    }
    public void CaracteristicaDisparo()
    {
        TiempoEntreDisparos = TiempoEntreDisparos + Time.deltaTime;
        if (TiempoEntreDisparos >= 1)
        {
            ArmaCargada = true;
            TiempoEntreDisparos = 0;
        }

    }
    //COMPORTAMIENTO
    public void Envenenamiento()
    {
        ContadorEnvenenamiento = ContadorEnvenenamiento + Time.deltaTime;

        if (ContadorEnvenenamiento >= 3)
        {
            Vida = Vida - 5;
            ContadorEnvenenamiento = 0;
        }


    }
    public void ElegirEquipo()
    {
        if (Input.GetKeyDown(KeyCode.Keypad0))
        {
            TipoArma = 0;
        }
        if (Input.GetKeyDown(KeyCode.Keypad1))
        {
            if (ArmaRecolectada == true)
            {
                TipoArma = 1;
            } // TIPO DE ARMA 1
        }

    }
    private void Equipamiento()
    {
        if (TipoArma == 1)
        {
            ComportamientoPistola();
        }
    }
    public void OrdenesMadre()
    {
        if (Madre.Liberada == true && Input.GetKeyDown(KeyCode.Q))
        {
            Madre.Seguir = !Madre.Seguir;
        }
    }
    public void Interacciones()
    {
        if (Physics.Raycast(Cam.position, Cam.forward, out InfoRayo, DistanciaRayo, ObjetosInteracciones))
        {
            InfoRayo.transform.GetComponent<Interactuable>().FuncionInteractuar();
        }
        Debug.DrawRay(Cam.position, Cam.forward * DistanciaRayo, Color.red);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Sangre"))
        {
            Vida = Vida + 25;
        } // cargar vida

        if (collision.gameObject.CompareTag("Chaleco"))
        {
            Armadura = Armadura + 25;
        } // cargar armadura

        if (collision.gameObject.CompareTag("Municion"))
        {
            Municion = Municion + 10;
            ArmaCargada = true;
        } // cargar municion

        if (collision.gameObject.CompareTag("Espina"))
        {
            Envenenado = true;
            if (Envenenado == true)
            {
                Vida = Vida - 5;
            }
        } // envenenarse

        if (collision.gameObject.CompareTag("Cura"))
        {
            Envenenado = false;
        } // curar veneno

        if (collision.gameObject.CompareTag("Pistola"))
        {
            ArmaRecolectada = true;
        }

        if (collision.gameObject.CompareTag("AtaqueEnemigo2"))
        {
            Vida = Vida - 15;
        }
        if (collision.gameObject.CompareTag("Enemigo2"))
        {
            Vida = Vida - 15;
        }

        if (collision.gameObject.CompareTag("Escapar"))
        {
            Nivel3.Ganaste = true;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Agua"))
        {
            ControlJugador3.Vida = 0;
        }
    }
}