using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuertaFuncional : MonoBehaviour
{
    public static bool Abierta;
    public bool InfoApertura;
    private Vector3 PosicionInicial;
    public Vector3 PosicionNueva; // SE VA DEFINIR EN EL INSPECTOR UNA VEZ SE SEPA DONDE SE COLOCARA LA PUERTA
       
    void Start()
    {
        PosicionInicial = gameObject.transform.position;        
    }

    
    void Update()
    {
        AbrirPuerta();
        InfoApertura = Abierta;
    }

    public void AbrirPuerta()
    {
        if (Abierta == false)
        {
         gameObject.transform.position = PosicionInicial;
        }
       
        if (Abierta == true)
        {
         gameObject.transform.position =  PosicionNueva;
        }
        }
    }

