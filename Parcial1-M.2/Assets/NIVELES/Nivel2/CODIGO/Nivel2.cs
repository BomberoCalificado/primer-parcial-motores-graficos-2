using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Nivel2 : MonoBehaviour
{
    public static Transform UbicarJugador;
    public static bool ActivarEnemigos;
    public float TiempoE;    
    public int ElegirEnemigo;

    public static bool Perdiste;
    public static bool Ganaste;


    void Start()
    {
        Perdiste = false;
        Ganaste = false;
        GameManager.SelectordeNivel = 2;
        UbicarJugador = ControlJugador2.UbicacionJugador;
    }

    void Update()
    {
        
        ControlNivel();
        TiempoPeligro();
        ElegirBot();
     
    }

    public void TiempoPeligro()
    {
        TiempoE = TiempoE + Time.deltaTime;

        if (TiempoE <= 3)
        {
            ElegirEnemigo = 1;
        }
        if (TiempoE >= 3 && TiempoE <= 6)
        {
            ElegirEnemigo = 2;
           
        }
        if (TiempoE >= 6 && TiempoE <= 9)
        {
            ElegirEnemigo = 3;
        }
        if (TiempoE >= 9)
        {
            TiempoE = 0;
        }

    } // CONTROLA EL TIEMPO DE ACTIVIDAD DE ENEMIGOS AL MISMO TIEMPO
    public void ControlNivel()
    {
        UbicarJugador = ControlJugador2.UbicacionJugador;

        if (ControlJugador2.VidaInfo <= 0)
        {
            Perdiste = true;
        }
        if (Ganaste == true)
        {
            Ganar();
        }
        if (Perdiste == true)
        {
            Perder();
        }


    }
    public void ElegirBot()
    {
        

        if (ElegirEnemigo == 1)
        {
            Enemigo2.Alerta = true;
        }           
        else { Enemigo2.Alerta = false; }

        if (ElegirEnemigo == 2)
        {
            Enemigo2B.Alerta = true;
        }           
        else { Enemigo2B.Alerta = false; }

        if (ElegirEnemigo == 3)
        {
         Enemigo2C.Alerta = true;
        }              
        else { Enemigo2C.Alerta = false; }



    } // CONTROLA EL TIEMPO DE ACTIVIDAD DE ENEMIGOS EN TIEMPO DIFENRENCIADOS

    public void Perder()
    {
        SceneManager.LoadScene("GameOver");
        
    }
    public void Ganar()
    {
        SceneManager.LoadScene("Winer");
    }
}
