using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tablero2 : MonoBehaviour
{
    public GameObject Boton1;
    public GameObject Boton2;
    public GameObject Boton3;

    public GameObject Interruptor;


    void Start()
    {

    }


    void Update()
    {
        CambiarColores();
        ActivarInterruptor();
    }

    private void CambiarColores()
    {
        if ( Boton1T2.Boton == true)
        { Boton1.transform.GetComponent<Renderer>().material.color = Color.green; }
        else { Boton1.transform.GetComponent<Renderer>().material.color = Color.red; }

        if (Boton2T2.Boton == true)
        { Boton2.transform.GetComponent<Renderer>().material.color = Color.green; }
        else { Boton2.transform.GetComponent<Renderer>().material.color = Color.red; }

        if (Boton3T2.Boton == true)
        { Boton3.transform.GetComponent<Renderer>().material.color = Color.green; }
        else { Boton3.transform.GetComponent<Renderer>().material.color = Color.red; }

        if (InterruptorT2.Estado == true)
        { Interruptor.transform.GetComponent<Renderer>().material.color = Color.green; }
        else { Interruptor.transform.GetComponent<Renderer>().material.color = Color.red; }

    }

    private void ActivarInterruptor()
    {
        if (Boton1T2.Boton == false && Boton2T2.Boton == false && Boton3T2.Boton == true)
        {
            InterruptorT2.Estado = true;
        }
    }
}
