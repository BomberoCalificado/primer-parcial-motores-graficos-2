using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tablero : MonoBehaviour
{
    public GameObject Boton1;
    public GameObject Boton2;
    public GameObject Boton3;

    public GameObject Interruptor;
    

    void Start()
    {
        
    }

    
    void Update()
    {
        CambiarColores();
        ActivarInterruptor();
    }

    private void CambiarColores()
    {
        if(Boton1T1.Boton == true)
        { Boton1.transform.GetComponent<Renderer>().material.color = Color.green; }
        else { Boton1.transform.GetComponent<Renderer>().material.color = Color.red; }

        if (Boton2T1.Boton == true)
        { Boton2.transform.GetComponent<Renderer>().material.color = Color.green; }
        else { Boton2.transform.GetComponent<Renderer>().material.color = Color.red; }

        if (Boton3T1.Boton == true)
        { Boton3.transform.GetComponent<Renderer>().material.color = Color.green; }
        else { Boton3.transform.GetComponent<Renderer>().material.color = Color.red; }

        if (InterruptorT1.Estado == true)
        { Interruptor.transform.GetComponent<Renderer>().material.color = Color.green; }
        else { Interruptor.transform.GetComponent<Renderer>().material.color = Color.red; }

    }

    private void ActivarInterruptor()
    {
     if (Boton1T1.Boton == true && Boton2T1.Boton == false && Boton3T1.Boton == true)
        {
            InterruptorT1.Estado = true;
        }
    }
}
