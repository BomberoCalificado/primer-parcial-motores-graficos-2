using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemigo2B : MonoBehaviour
{
    // RECURSOS DE ATAQUE
    public float VelocidadGiro;
    public float ControladorApertura;
    public bool BrazosCerrados;

    // RECURSOS DE ESTADO
    public static bool Alerta;

    //MOVIMIENTO
    public float VelocidadMov;


    void Start()
    {

    }

    void Update()
    {
  
        if (Alerta == true)
        {
            Perseguir();
        }
        else { gameObject.transform.GetComponent<Renderer>().material.color = Color.black; }
    }

    private void Moverbrazos()
    {
        ControlarBrazos();
        if (BrazosCerrados == false) // ABRIR BRAZOS
            BrazoIzqB.BrazoIzquierdo.transform.Rotate(new Vector3(0, -1, 0) * VelocidadGiro * Time.deltaTime);
        if (BrazosCerrados == true) // CERRAR BRAZOS
            BrazoIzqB.BrazoIzquierdo.transform.Rotate(new Vector3(0, +1, 0) * VelocidadGiro * Time.deltaTime);

        if (BrazosCerrados == false) // ABRIR BRAZOS
            BrazoDerB.BrazoDerecho.transform.Rotate(new Vector3(0, +1, 0) * VelocidadGiro * Time.deltaTime);
        if (BrazosCerrados == true) // CERRAR BRAZOS
            BrazoDerB.BrazoDerecho.transform.Rotate(new Vector3(0, -1, 0) * VelocidadGiro * Time.deltaTime);


    }

    public void ControlarBrazos()
    {
        ControladorApertura = ControladorApertura + Time.deltaTime;
        if (ControladorApertura <= 1.30f)
        {
            BrazosCerrados = false;

        }
        if (ControladorApertura >= 1.30f && ControladorApertura <= 2.60f)
        {
            BrazosCerrados = true;
        }
        if (ControladorApertura > 2.60)
        {
            ControladorApertura = 0;
        }
    }

    public void Perseguir()
    {

        gameObject.transform.GetComponent<Renderer>().material.color = Color.red;
        transform.LookAt(Nivel2.UbicarJugador); // mantener la mirada hacia el jugador
        transform.position = Vector3.MoveTowards(transform.position, new Vector3(Nivel2.UbicarJugador.position.x, Nivel2.UbicarJugador.position.y, Nivel2.UbicarJugador.position.z), VelocidadMov * Time.deltaTime);
        Moverbrazos();
    } // si estoy alerta observo al enemigo
}
