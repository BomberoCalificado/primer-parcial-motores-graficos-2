using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Puerta : MonoBehaviour
{
    public static bool Estado;

    void Start()
    {
        
    }

 
    void Update()
    {
        ActivarPuerta();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (Estado == true && collision.gameObject.CompareTag("Player"))
        {
            Nivel2.Ganaste = true;
        }
    
}

    private void ActivarPuerta()
    {
        if (InterruptorT1.Estado == true && InterruptorT2.Estado == true && InterruptorT3.Estado == true)
        {
            Estado = true;
            transform.GetComponent<Renderer>().material.color = Color.green;
        }
        else { Estado = false;
             transform.GetComponent<Renderer>().material.color = Color.red;
        }
    }
}
