using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlJugador : MonoBehaviour
{
    public Rigidbody Rb;
    public float Fuerza;
    public float Velocidad;
    public Vector2 Smouse;
    public float VelocidadMovimiento;
    public Transform Cam;
    public static float Vida;

   
   
    void Start()
    {
        Rb = GetComponent<Rigidbody>();
        Cursor.lockState = CursorLockMode.Locked; // hace que el cursos no se mueva por toda la pantalla, osea queda fijo en una posicion
    }

 
    void Update()
    {
         MoverJugador();
         RotacionJugador();
    }

    public void MoverJugador()
    {
        float H = Input.GetAxisRaw("Horizontal");
        float V = Input.GetAxisRaw("Vertical");
        Vector3 Velocidades = Vector3.zero;

        if (H != 0 || V != 0) // si algun valor es diferente a 0 entonces el jugador se esta moviendo
        {
            Vector3 Direccion = (transform.forward * V + transform.right * H).normalized;
            Velocidades = Direccion * VelocidadMovimiento * Time.deltaTime;
        }
        Velocidades.y = Rb.velocity.y;
        Rb.velocity = Velocidades; 

    }

    public void RotacionJugador()
    {
        float RotacionH = Input.GetAxis("Mouse X");
        float RotacionV = Input.GetAxis("Mouse Y");
        if (RotacionH != 0)
        {
            transform.Rotate(0, RotacionH * Smouse.x * Time.deltaTime , 0);
        }
        if (RotacionV != 0)
        {
            Cam.Rotate(-RotacionV * Smouse.y * Time.deltaTime, 0, 0);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Cuchilla"))
        {
            Vida = Vida - 50;
        }
    }
}
