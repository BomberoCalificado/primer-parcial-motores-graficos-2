using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class Nivel1 : MonoBehaviour
{
    public Transform Jugador;
    public static Transform UbicarJugador;
    

    public static bool Ganaste;
    public static bool Perdiste;
    public static bool  GanarNivel;
    void Start()
    {
        ControlJugador.Vida = 50;
        Enemigo1.Vida = 50;
        Ganaste = false;
        Perdiste = false;        
        GameManager.SelectordeNivel = 1;
        
    }

 
    void Update()
    {
        UbicarJugador = Jugador; // referencia accesible de ubicacion del jugador
        ControlNivel();
    }

    public void ControlNivel()
    {
       

        if (ControlJugador.Vida <= 0)
        {
            Perdiste = true;
        }
        if (Enemigo1.Vida <= 0)
        {
            Ganaste = true;
        }
        if (Ganaste == true)
        {
            Ganar();
        }
        if (Perdiste == true)
        {
            Perder();
        }

    }

    public void Perder()
    {
        SceneManager.LoadScene("GameOver");

    }
    public void Ganar()
    {
        SceneManager.LoadScene("Winer");
    }
}
