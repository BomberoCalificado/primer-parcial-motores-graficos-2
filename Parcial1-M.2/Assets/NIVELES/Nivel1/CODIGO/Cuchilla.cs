using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cuchilla : MonoBehaviour
{
    //RECURSOS MOVIMIENTO
    public float VelocidadGiro;
    private Rigidbody Rb;
    public float Fuerza;
    public float Velocidad;
    public float TiempoUso;
   
   


    void Start()
    {
        Rb = GetComponent<Rigidbody>();
        transform.LookAt(Nivel1.UbicarJugador); // APUNTA AL JUGADOR UNA UNICA VEZ AL CREARSE
    
    }


    void Update()
    {
        Mover();
        Girar();
        VidaUtil();
        
    }

    private void Girar()
    {
        transform.Rotate(new Vector3(0, 0, 1) * VelocidadGiro * Time.deltaTime);
    }

    private void Mover()
    {
        Rb.AddForce(transform.forward * Velocidad * Time.deltaTime);
      
    }
    private void VidaUtil() {
        TiempoUso = TiempoUso - Time.deltaTime;
        if (TiempoUso <= 0)
        Destroy(gameObject);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            Destroy(gameObject, 0.3f);
        }

        if (collision.gameObject.CompareTag("Enemigo1"))
        {
            Destroy(gameObject);
        }
    }
}
