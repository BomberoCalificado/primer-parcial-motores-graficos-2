using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemigo1 : MonoBehaviour
{
    // RECURSOS DE ATAQUE
    public Transform Instanciador; // punto de referencia
    public GameObject Arma; // que tipo de arma va usar
    public bool Yadispare;

    // RECURSOS DE TELETRANSPORTE
    public float TiempoMovimiento; // Controla el tiempo entre teletrasportes
    public bool Moverse; // controla la orden moverse
    public Vector3 NuevaPosicion; // es la nueva ubicacion donde se va a teletransportar el enemigo
    public float X;
    public float Z;

    // RECURSOS BASICOS
    public static float Vida;
    public float VidaInfor;
  
    void Start()
    {
        Vida = 150;
    }

    void Update()
    {
        VidaInfor = Vida;
        transform.LookAt(Nivel1.UbicarJugador); // mantener la mirada hacia el jugador
        ControlTiempo();
        Teletransportarse();
        Disparar();
        if (Vida <= 0)
        {
            Destroy(gameObject, 05f);
        }
    }

    public void Teletransportarse()
    {
        X = Random.Range(-14,10);
        Z = Random.Range(16, -16);
        NuevaPosicion = new Vector3(X, 2, Z); // creamos la nueva posicion aleatoria

        if (Moverse == true)
        {
            transform.position = NuevaPosicion;
            TiempoMovimiento = 0;
            Moverse = false;
            Yadispare = false;
        } // cambiamos la posicion del personaje

    }

    public void ControlTiempo()
    {
        TiempoMovimiento = TiempoMovimiento + Time.deltaTime;
        if (TiempoMovimiento >= 5)
        {
            Moverse = true;            
        }
    
    }

    public void Disparar()
    {
        if (Moverse == false && Yadispare == false)
        {
            Instantiate(Arma, Instanciador.position, Quaternion.identity);
            Yadispare = true;
        }
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Pinchos"))
        {
            Vida = Vida - 25;
        }
    }
    }
